import struct
import socket
import datetime
from datetime import datetime as dateclass
import time
import math

SNTP_DIFFERENCE = (datetime.date(*time.gmtime(0)[0:3]) - datetime.date(1900, 1, 1)).days * 24 * 3600

with open("config.txt", 'r', encoding="utf-8") as file:
    LIE_TIME = int(file.readline().rstrip("\n"))


def _to_fraction(timestamp):
    return int((timestamp - int(timestamp)) * 2 ** 32)


class SNTPPacket:
    def __init__(self):
        self.leap_indicator = 3
        self.version = 0
        self.mode = 0
        self.stratum = 2
        self.poll = 0
        self.precision = -int(math.log2(32))
        self.root_delay = 0
        self.root_dispersion = 0
        self.reference_id = 0
        self.reference_timestamp = 0
        self.originate_timestamp = 0
        self.receive_timestamp = 0
        self.transmit_timestamp = 0

    def initilize(self, data, receive_time, mode=4):
        unpacked_data = struct.unpack("!3Bb11I", data)
        self.leap_indicator = unpacked_data[0] >> 6 & 0x3
        self.version = unpacked_data[0] >> 3 & 0x7
        self.mode = mode
        self.poll = unpacked_data[2]
        self.reference_id = unpacked_data[6]
        self.reference_timestamp = receive_time + SNTP_DIFFERENCE
        self.originate_timestamp = unpacked_data[10] + (unpacked_data[11]/(2**32))
        self.receive_timestamp = receive_time + SNTP_DIFFERENCE + LIE_TIME
        self.transmit_timestamp = dateclass.timestamp(dateclass.utcnow()) + SNTP_DIFFERENCE + LIE_TIME

    def pack(self):
        return struct.pack("!3Bb11I",
                           (self.leap_indicator << 6 | self.version << 3 | self.mode),
                           self.stratum,
                           self.poll,
                           self.precision,
                           self.root_delay,
                           self.root_dispersion,
                           self.reference_id,
                           int(self.reference_timestamp),
                           _to_fraction(self.reference_timestamp),
                           int(self.originate_timestamp),
                           _to_fraction(self.originate_timestamp),
                           int(self.receive_timestamp),
                           _to_fraction(self.receive_timestamp),
                           int(self.transmit_timestamp),
                           _to_fraction(self.transmit_timestamp))


if __name__ == "__main__":
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.bind(("127.0.0.1", 123))
    while True:
        print("Waiting...")
        server.settimeout(60)
        try:
            data, address = server.recvfrom(1024)
        except socket.timeout:
            print("Socket timeout passed.({0} seconds)".format(server.timeout))
            break
        receive_time = dateclass.timestamp(dateclass.utcnow())
        packet = SNTPPacket()
        packet.initilize(data, receive_time)
        reply = packet.pack()
        server.sendto(reply, address)
    server.close()
